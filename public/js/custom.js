  io.connect();
    $(() => {
       // getchat(chats)
       var socket = io();
        $("#send").click(() => { 
            postChat();
          })
        $('#txtMessage').keydown(function(e) {
            if(e.which == 10 || e.which == 13) {
               postChat();
            }   
        });
        function postChat(){
            var chatMessage = {
                receiverId: $("#receiverid").val(),
                senderId: $("#current_userid").val(),
                message: $("#txtMessage").val()
            }
            socket.emit('message', chatMessage);
            $("#txtMessage").val("");
        }
        socket.on("receiveMessage", (data) => {
            console.log("socket emitted .......");
            console.log($("#current_userid").val().toString(), data.senderId.toString());
            let receiver = $('#receiver_username').val();
            let sender = $('#current_username').val();
            
            if($("#current_userid").val().toString() == data.senderId.toString()) {
                $("#chat").append(`<li class="list-group-item   ">
              <div class="row ">
                <div class="col-xs-10 col-md-11 ">
                  <div class="bubble2">${data.message} </div>
                </div>
              </div>
            </li>`);
            }
            if($("#current_userid").val().toString() == data.receiverId.toString()){
                $("#chat").append(`<li class="list-group-item bubble">
                  <div class="row ">
                    <div class="col-xs-10 col-md-11 ">
                      <div class="ro">
                        <h4 class="list-group-item-heading ">${receiver} :</h4>
                        <p class="list-group-item-text">${data.message}</p>
                      </div>
                    </div>
                  </div>
                </li>`);
            }
        });
    });