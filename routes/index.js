var User = require('../models/user');
var Message = require('../models/message');

module.exports = function(app, passport) {
    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage') });
    });
    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    // process the login form
    // app.post('/login', do all our passport stuff here);

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });
    // process the signup form
    // app.post('/signup', do all our passport stuff here);
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/profile', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    
    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/profile', isLoggedIn, async function(req, res) {
        let userid = req.user._id;
        let usersData = await User.find({ 'local.status': 1, '_id': { $ne: userid } });
        res.render('profile.ejs', {
            user: req.user, // get the user out of session and pass to template
            usersData: usersData
        });
    });

    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        var userid = req.user._id
        console.log(userid);
        User.findOneAndUpdate({ _id: userid }, { 'local.status': 0 }, function(err, doc) {
            console.log(err, doc);
        });
        req.logout();
        res.redirect('/');
    });
    // =====================================
    // CHAT ==============================
    // =====================================
    app.get('/chat/:userid', isLoggedIn, async function(req, res) {
        var chatUID = req.params.userid; //get chat id pass from profile chat button
        var receiver = await User.findOne({ _id: chatUID });
        var userid = req.user._id; //get current user id
        Message.find({}).populate(["receiverId", "senderId"]).exec((err, messages) => {
            res.render('chat.ejs', {
                sender: req.user, // get the user out of session and pass to template
                receiver: receiver,
                messages: messages
            });
        });

    });
};
////End Chat///

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}