var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var port = process.env.PORT || 8080;
var app = express();
const http = require('http')
const socketIO = require('socket.io')
const server = http.createServer(app)
const io = socketIO(server)
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Message = require('./models/message');
var session = require('express-session');

var configDB = require('./config/database.js');

io.on('connection', (socket) => {
    console.log('User connected')
    socket.on("message", async(message) => {
        io.emit('receiveMessage', message);
        try {
            var chat = new Message(message)
            chat.createdAt = new Date().toString();
            await chat.save();
        } catch (error) {
            console.error(error);
        }
    })
    socket.on('disconnect', () => {
        console.log('user disconnected')
    })
})

// configuration ===============================================================x

// mongoose.connect(configDB.url ,{ useMongoClient: true, /* other options */ }); // connect to our database
mongoose.connect(configDB.url, { useMongoClient: true }).then(
    () => { console.log('Connected!!!') },
    err => { console.log(err); }
  );
require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({
    extended: true
  }));
app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./routes')(app, passport); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
server.listen(process.env.NODE_ENV|3000, function() {
    console.log('listening on *:3000');
});

module.exports = app;