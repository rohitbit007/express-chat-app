var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the schema for our user model
var messageSchema = mongoose.Schema({
    receiverId: { type: String, ref: "User" },
    senderId: { type: String, ref: "User" },
    message: String,
    createdAt: Date
});

// create the model for users and expose it to our app
module.exports = mongoose.model('Message', messageSchema);